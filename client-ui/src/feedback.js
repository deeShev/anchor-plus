import React from "react"
import {connect} from 'react-redux'
import {setName} from "./features/redux/Reducer"
import {fetchClientName, submit} from "./features/redux/actions";
import Button from '@material-ui/core/Button';
import Dialog from "@material-ui/core/Dialog";
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import {MdMood, MdMoodBad, MdSentimentDissatisfied, MdSentimentSatisfied} from "react-icons/all";
import {Input} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const Feedback = (props) => {
    const [rank, setRank] = React.useState('3');
    const [comment, setComment] = React.useState();

    const handleRank = (event, newRank) => {
        setRank(newRank);
    };

    return <Dialog open={props.isMeetingComplete} className={"justText"} fullWidth={true}>
        <div style={{padding: "20px", display: "block !important"}}>
            <h2 align={"center"}>Пожалуйста, оцените наш сервис</h2>
            <div style={{textAlign: "center", display: "block !important"}}>
                <ToggleButtonGroup
                    value={rank}
                    exclusive
                    onChange={handleRank}
                >
                    <ToggleButton size={"large"} value="0"
                                  style={{fontSize: "2em", border: 0, borderRadius: 0, margin: "3px"}}>
                        <MdMoodBad/>
                    </ToggleButton>
                    <ToggleButton size={"large"} value="1" style={{fontSize: "2em", border: 0, margin: "3px"}}>
                        <MdSentimentDissatisfied/>
                    </ToggleButton>
                    <ToggleButton size={"large"} value="2" style={{fontSize: "2em", border: 0, margin: "3px"}}>
                        <MdSentimentSatisfied/>
                    </ToggleButton>
                    <ToggleButton size={"large"} value="3"
                                  style={{fontSize: "2em", border: 0, borderRadius: 0, margin: "3px"}}>
                        <MdMood/>
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>
            <div style={{height: "10px"}}/>
            <Input multiline={true} fullWidth={true} className={"sberId3"} disableUnderline={true}
                   rowsMax={5}
                   value={comment}
                   onChange={(e) => setComment(e.target.value)}
                   placeholder={" Комментарий"}
                   style={{fontFamily: "sans-serif !important"}}
                   rows={4}
                   rowsMin={3}/>
            <div style={{height: "10px"}}/>
            <Grid container style={{justifyContent: 'center'}}>
                <Grid item xl={4} xs={4}/>
                <Grid item xl={4} xs={4}>
                    <Button className={"sberId4"} onClick={()=> props.doSubmit(rank, comment, props.meeting.id)}>Отправить</Button>
                </Grid>
                <Grid item xl={4} xs={4}/>
            </Grid>
        </div>
    </Dialog>
};

const mapStateToProps = (state, ownProps) => {
    return {
        text: state.main.text,
        isMeetingComplete: state.main.isMeetingComplete,
        meeting: state.main.meeting
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doSubmit: (rank, comment, meetingId) => {
            dispatch(submit({rank, comment, meetingId}));
            ownProps.history.push('/client-ui');
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Feedback)

