import axios from "axios"

export function getName() {
    return axios.get('/client/name')
}

export function getProducts() {
    return axios.get('/core/products')
}

export function doLogin(login) {
    return axios.get('/core/client-by-login?login=' + login);
}

export function doSubmit(csi, comment, meetingId) {
    return axios.post('/core/feedback-save', {csi, comment, meetingId});
}

export function getMeetings(clientId) {
    return axios.get('/core/meeting-by-client-id?id=' + clientId);
}

export function doScheduleMeeting(clientId, productId, dateTime) {
    return axios.get('/core/meeting-get-random-empl?clientId=' + clientId + "&productId=" + productId
        + "&dateTime=" + (dateTime || ""));
}

export function doSaveMeeting(clientId, productId, employeeId, dateTime, comment) {
    return axios.post('/core/meeting-save-dto-random/', {clientId, productId, employeeId, dateTime, comment});
}