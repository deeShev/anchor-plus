import React, {useEffect} from "react"
import {connect} from 'react-redux'
import {fetchProducts} from "./features/redux/actions";
import Header from "./header/header"
import Banner from "./header/sberBanner";
import {Route, Switch} from 'react-router-dom'
import ForMe from "./forMe/forMe";
import ForBusiness from "./forBusiness/forBusiness";
import Grid from "@material-ui/core/Grid";

const Main = (props) => {
    useEffect(() => {
        props.getProducts();
    }, []);

    return <div>
        <Header/>
        <div style={{paddingTop: '64px'}}>
            <Banner/>
            <Grid container style={{justifyContent: 'center'}}>
                <Grid item xl={8} xs={10}>
                    <Switch>
                        <Route path={'/client-ui/for-business'} strict  component={ForBusiness}/>
                        <Route path={'/client-ui/'} component={ForMe} />
                    </Switch>
                </Grid>
            </Grid>
        </div>
    </div>
};

const mapStateToProps = (state, ownProps) => {
    return {
        text: state.main.text
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getProducts: () => {
            dispatch(fetchProducts());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Main)

