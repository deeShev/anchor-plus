import React, {useState} from "react"
import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import mainLogo from "../img/sber_logo_main.png";
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import {scheduleMeeting} from "../features/redux/actions";
import {connect} from "react-redux";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Button from "@material-ui/core/Button";
import VideocamIcon from "@material-ui/icons/Videocam"
import Dialog from "@material-ui/core/Dialog";
import {endMeeting, logout} from "../features/redux/Reducer";
import DialogContent from "@material-ui/core/DialogContent";
import {VideoRoom} from "../video/VideoRoom";
import Feedback from "../feedback";
import Calendar from "../calendar/Calendar";
import CreateDialog from "../calendar/createDialog";


const Booking = (props) => {
    const [open, setOpen] = useState();

    return <div>
        <Dialog fullScreen
                open={!!(props.isMeetingAvailable && props.meeting.id)}
                onClose={props.doEndMeeting}>
            <DialogContent>
                <VideoRoom meeting={props.meeting} userId={props.user.id}/>
            </DialogContent>
        </Dialog>
        <Feedback meeting={props.meeting} history={props.history}/>
        <AppBar position="fixed" className={'app-bar'}>
            <Grid container style={{justifyContent: 'center'}}>
                <Grid item xl={8} xs={10}>
                    <Toolbar>
                        <div
                            color="inherit"
                            aria-label="open drawer"
                            onClick={() => {
                            }}
                            className={'header-menu'}/>
                        <Link to={'/client-ui'}>
                            <img src={mainLogo} className={'clickable'} style={{width: "105px", margin: "15px"}}
                                 alt="fireSpot"/>
                        </Link>

                        <div style={{flex: '1 0', display: 'flex', justifyContent: 'flex-end', alignItems: 'center'}}>
                            <Typography variant="body1">
                                {props.user.firstName}
                            </Typography>
                        </div>
                        <ExitToAppIcon className={'icon px10'} onClick={() => props.doLogout(props)}/>
                    </Toolbar>
                </Grid>
            </Grid>
        </AppBar>

        <CreateDialog open={open} closeFunc={() => setOpen(false)}/>
        <div style={{paddingTop: '64px'}}>
            <Typography variant={'h3'}>{props.product.name}</Typography>
            <Typography variant={'h4'}>Получить видео консультацию</Typography>
            <div className={'pgy30'} style={{display: "inline-flex"}}>
                <Button className={"now"} onClick={() => {
                    props.doScheduleMeeting(props.user.id, props.product.id, (new Date()).getTime())
                }}><span className={'pg10'}>Прямо сейчас</span> {<VideocamIcon className={'pg10'}/>}</Button>
                <div style={{paddingRight: "10px"}}/>
                <Button className={"now"} onClick={() => {
                    setOpen(true);
                }}><span className={'pg10'}>Запланировать</span></Button>
            </div>
            <Typography>{props.isMeetingAvailable === false ? 'Нет свободных операторов, вы можете запланировать консультацию на удобное вам время и мы обязательно свяжемся с вами' : ''} </Typography>
        </div>
        <Calendar/>
    </div>
};

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.main.user,
        meeting: state.main.meeting,
        isMeetingAvailable: state.main.isMeetingAvailable,
        product: state.main.product,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doScheduleMeeting: (clientId, productId, dateTime) => {
            dispatch(scheduleMeeting({clientId, productId, dateTime}));

        },
        doLogout: () => {
            dispatch(logout());
            ownProps.history.push("/client-ui");

        },
        doEndMeeting: () => {
            dispatch(endMeeting());
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Booking);