import React, {useEffect} from "react"
import ProductToolbar from "../product/ProductToolbar";
import Categories from "../product/Categories";
import {fetchProducts} from "../features/redux/actions";
import {connect} from "react-redux";

const ForMe = (props) => {
    return <div>
        <ProductToolbar products = {props.physProductsByCategory}/>
        <Categories products = {props.physProductsByCategory} history = {props.history}/>
    </div>
};

const mapStateToProps = (state, ownProps) => {
    return {
        products: state.main.products,
        physProductsByCategory: state.main.physProductsByCategory,
        corProductsByCategory: state.main.corProductsByCategory,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getProducts: () => {
            dispatch(fetchProducts());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ForMe)