import {fetchMeetings, saveMeeting} from "../features/redux/actions";
import {connect} from "react-redux";
import React, {useState} from "react"
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {Input} from "@material-ui/core";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "@material-ui/core/TextField";

const CreateDialog = (props) => {
    const [comment, setComment] = useState();
    const [dateTime, setDateTime] = useState();
    return <Dialog open={props.open} className={"justText"} fullWidth={true}>
        <div style={{padding: "20px", display: "block !important"}}>
            <h2 align={"center"}>{props.product.name}</h2>
            <div style={{textAlign: "left", display: "block !important"}}>
                <TextField
                    id="datetime-local"
                    label="Дата и время встречи"
                    type="datetime-local"
                    value={dateTime || ""}
                    onChange={(e) => setDateTime(e.target.value || '')}

                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <div style={{height: "10px"}}/>
                <Input multiline={true} fullWidth={true} className={"sberId3"} disableUnderline={true} rowsMax={5}
                       placeholder={" Комментарий"}
                       value={comment}
                       onChange={(e) => setComment(e.target.value)}
                       style={{fontFamily: "sans-serif !important"}}
                       rows={4}
                       rowsMin={3}/>
                <DialogActions>
                    <Button size="small" style={{fontFamily: "sans-serif"}} onClick={props.closeFunc}>Отменить</Button>
                    <Button size="small"
                            onClick={() => {
                                let d =  new Date(dateTime);
                                console.log({u: props.user.id, p: props.product.id, e: null, d: d, comment});
                                props.createMeeting(props.user.id, props.product.id, null, d.getTime(), comment);
                                props.closeFunc();
                            }}
                            style={{
                                backgroundColor: "#19bb4f",
                                color: '#fff',
                                fontFamily: "sans-serif"
                            }}>Создать</Button>
                </DialogActions>
            </div>
        </div>
    </Dialog>
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.main.user,
        meeting: state.main.meeting,
        product: state.main.product
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

        createMeeting: (clientId, productId, employeeId, dateTime, comment) => {
            dispatch(saveMeeting({clientId, productId, employeeId, dateTime, comment}))
                .then(e => {
                    dispatch(fetchMeetings(clientId));
                    return e;
                });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateDialog)