import {fetchClientName} from "../features/redux/actions";
import {setName} from "../features/redux/Reducer";
import {connect} from "react-redux";
import React from "react"
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";

const MeetingCard = (props) => {
    return <Card variant="outlined">
        <CardContent>
            <Typography style={{textAlign: "left"}} gutterBottom>
                {new Date(props.meeting.dateTime).toLocaleDateString()} {new Date(props.meeting.dateTime).toLocaleTimeString()}
            </Typography>
            <Typography variant="h5" component="h5" style={{textAlign: "left"}}>
                <div>Продукт: {props.meeting.product.name}</div>
            </Typography>
            <Typography variant="h5" component="h3" style={{textAlign: "left"}}>
                <div>Консультант: {props.meeting.employee.lastName} {props.meeting.employee.firstName}</div>
            </Typography>
        </CardContent>
        <CardActions>
            <Button size="small" style={{fontFamily: "sans-serif"}}>Отменить</Button>
            <Button size="small"
                    style={{backgroundColor: "#19bb4f", color: '#fff', fontFamily: "sans-serif"}}>Подключиться</Button>
        </CardActions>
    </Card>
}

const mapStateToProps = (state, ownProps) => {
    return {
        text: state.main.text
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getName: () => {
            dispatch(fetchClientName());
        },
        onChange: (e) => {
            dispatch(setName(e.target.value));
            console.log(e.target.value);
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetingCard)