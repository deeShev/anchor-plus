import React, {useEffect} from "react"
import {fetchMeetings} from "../features/redux/actions";
import {connect} from "react-redux";
import MeetingCard from "./card";


const Calendar = (props) => {
    useEffect(() => {
        props.doFetchMeetings(props.user.id);
    }, []);
    return <div>
        {(props.meetings || []).map((i) => <div key={i.id}  className={"flex-center-center pg10"}>
            <MeetingCard meeting={i}/>
        </div>)}
        {/*{JSON.stringify(props.meetings || [])}*/}
    </div>
};

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.main.user,
        meeting: state.main.meeting,
        meetings: state.main.meetings,
        isMeetingAvailable: state.main.isMeetingAvailable,
        product: state.main.product,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doFetchMeetings: (id) => {
            dispatch(fetchMeetings(id));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);