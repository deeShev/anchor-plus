import React from "react"
import {connect} from 'react-redux'
import SberIdHeader from "./header/sberIdHeader"

const Login = (props) => {
    return <div>
        <SberIdHeader {...props}/>
    </div>
};

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.main.user
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        loginUsr: () => {
            if (!ownProps.user.login) {
                ownProps.history.push("/client-login");
            }
            ownProps.history.push("/booking");
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login)

