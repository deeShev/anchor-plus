import React, {useState} from 'react';
import {Button, Grid} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";

const VideoPlaceholder = (props) => {

    return (
    <Grid container alignItems="center" justify="center" style={{ minHeight: '100vh' }}>
        <Grid item xs={12}>
            <Typography variant="body1" noWrap style={{padding: '0px 20px'}}>
                {
                    props.loading ?
                        <div>
                            <CircularProgress color='primary'/>
                            <br/>
                            {props.text}
                        </div>
                        :
                        props.text
                }
            </Typography>
        </Grid>
    </Grid>

    )
}

export default VideoPlaceholder;