import React, {useEffect, useRef, useState} from 'react'


export default function Video(props) {
    const refVideo = useRef(null)

    useEffect(() => {
        if (!refVideo.current) return
        refVideo.current.srcObject = props.srcobject
    }, [props.srcobject])


    return <video autoPlay muted ref={refVideo} {...props} />
}
