import React, {useEffect, useState} from 'react';
import {VideoRoom} from "./VideoRoom";
import {useParams} from "react-router";
import VideoPlaceholder from "./VideoPlaceholder";
import { useSelector, useDispatch } from 'react-redux';
import {
    getMeeting,
    selectAppointment,
} from './slice/videoPageSlice';

function VideoPage() {
    const appointment = useSelector(selectAppointment);
    const {id} = useParams();
    const {user} = useParams();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getMeeting(id));
    }, [])

    const getPlaceholderText = () => {
        if (!appointment) {
            return 'Загружаем встречу';
        }

        if (!ready(appointment)) {
            return `Встреча начнется в ${appointment.time}. Пожалуйста, подключитесь позже`
        }

        function ready(appointment) {
            return true;
        }
    }

    const placeholderText = getPlaceholderText();

    return (
        <div>
            {placeholderText ?
                <VideoPlaceholder loading={!appointment} text={placeholderText} />
                :
                <VideoRoom meeting={appointment} userId={user}/>
            }
        </div>
    );
}

export default VideoPage;