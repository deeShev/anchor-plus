import React, {useState} from 'react';
import Video from "./Video";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import '../css/video.css'
import CardActions from "@material-ui/core/CardActions";
import {
    Fullscreen,
    FullscreenExit,
    Mic,
    MicOff,
    ScreenShare,
    StopScreenShare,
    Videocam,
    VideocamOff
} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import {FullScreen, useFullScreenHandle} from "react-full-screen";
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import {VideoChat} from "./VideoChat";
import {scheduleMeeting} from "../features/redux/actions";
import {endMeeting, logout} from "../features/redux/Reducer";
import {connect} from "react-redux";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";

function VideoPlayer(props) {

    const [audioEnabled, setAudioEnabled] = useState(true);
    const [videoEnabled, setVideoEnabled] = useState(true);
    const [screenShared, setScreenShared] = useState(false);

    const fullScreenHandle = useFullScreenHandle();


    const audioOn = () => {
        props.localStream.getAudioTracks()[0].enabled = true;
        setAudioEnabled(true);
    }

    const audioOff = () => {
        props.localStream.getAudioTracks()[0].enabled = false;
        setAudioEnabled(false);
    }

    const videoOn = () => {
        props.localStream.getVideoTracks()[0].enabled = true;
        setVideoEnabled(true);
    }

    const videoOff = () => {
        props.localStream.getVideoTracks()[0].enabled = false;
        setVideoEnabled(false);
    }


    const shareScreen = () => {
        const {peerConnection} = props;
        const displayMediaOptions = {
            cursor: 'always'
        }
        navigator.mediaDevices.getDisplayMedia(displayMediaOptions).then(
            stream => {
                const sender = peerConnection.getSenders().find(sender => sender.track.kind === 'video');
                console.log('found sender:', peerConnection.getSenders());
                sender.replaceTrack(stream.getVideoTracks()[0]);
                setScreenShared(true);
            }).catch(
            error => {
                console.log(error)
            });
    }

    const stopScreenSharing = () => {
        const {peerConnection} = props;
        const sender = peerConnection.getSenders().find(sender => sender.track.kind === 'video');
        console.log('found sender:', peerConnection.getSenders());
        sender.replaceTrack(props.localStream.getVideoTracks()[0]);
        setScreenShared(false);
    }


    const finishMeeting = () => {

    }

    return (
        <FullScreen handle={fullScreenHandle}>
            <Card className={'player'}>
                <CardContent>
                    <div className={'video-container '} >

                        {!fullScreenHandle.active &&
                        <Video className={props.remoteStream ? 'localVideo' :'localVideo-inactive'} srcobject={props.localStream} />}
                        <Video className={'remoteVideo'}
                               style={fullScreenHandle.active ? {width: '75%'} : {}}
                               srcobject={props.remoteStream}>

                        </Video>
                        }
                    </div>
                </CardContent>
                <CardActions>
                    <div className={'flex-center-center'} style={{width: '100%'}}>

                            <IconButton color="primary" component="span" onClick={audioEnabled ? audioOff : audioOn}>
                                {audioEnabled ? <Mic/> : <MicOff/>}
                            </IconButton>
                            <IconButton color="primary" component="span" onClick={videoEnabled ? videoOff : videoOn}>
                                {videoEnabled ? <Videocam/> : <VideocamOff/>}
                            </IconButton>
                            <IconButton color="primary" component="span"
                                        onClick={screenShared ? stopScreenSharing : shareScreen}>
                                {screenShared ? <StopScreenShare/> : <ScreenShare/>}
                            </IconButton>
                            <IconButton color="primary" component="span"
                                        onClick={fullScreenHandle.active ? fullScreenHandle.exit : fullScreenHandle.enter}>
                                {fullScreenHandle.active ? <FullscreenExit/> : <Fullscreen/>}
                            </IconButton>

                        <div >
                            <Button onClick={props.doEndMeeting}  style={{color : 'red'}}>
                                Завершить
                            </Button>
                        </div>
                    </div>
            </CardActions>
        </Card>
        </FullScreen>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.main.user,
        meeting: state.main.meeting,
        isMeetingAvailable: state.main.isMeetingAvailable,
        product: state.main.product,
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doScheduleMeeting: (clientId, productId, dateTime) => {
            dispatch(scheduleMeeting({clientId, productId, dateTime}));

        },
        doLogout: () => {
            dispatch(logout());
            ownProps.history.push("/client-ui");

        },
        doEndMeeting: () => {
            dispatch(endMeeting());
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VideoPlayer);