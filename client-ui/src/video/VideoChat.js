import React, {useEffect, useState} from "react";
import Paper from "@material-ui/core/Paper";
import {Grid} from "@material-ui/core";
import Chip from "@material-ui/core/Chip";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import {Send} from "@material-ui/icons";
import Input from "@material-ui/core/Input";

import '../css/video.css'

export function VideoChat(props) {

    const [currentMessage, setCurrentMessage] = useState('');
    const [messages, setMessages] = useState([]);

    const getMessages = () => {
        return messages.map((item, index) =>
            <div className={'chat-message'} style={{'textAlign': item.local ? 'right' : "left"}}>
                <Chip key={index} label={item.message} color={item.local ? 'primary' : 'default'}></Chip>
            </div>
        )
    }

    const sendMessage = () => {
        props.datachannel.send(currentMessage);
        addLocalMessage({local: true, message: currentMessage})
        setCurrentMessage('')
    }

    const onMessage = (event) => {
        console.log('onMessage')
        addLocalMessage({local: false, message: event.data})
    }

    const addLocalMessage = (message) => {
        setMessages(prev => [...prev, message])
    }

    useEffect(() => {
        const {datachannel} = props;
        if (!datachannel) {
            return;
        }
        datachannel.onerror = function (error) {
            console.log("Error occured on datachannel:", error);
        };

        // when we receive a message from the other peer, printing it on the console
        datachannel.onmessage = onMessage;

        datachannel.onclose = function () {
            console.log("data channel is closed");
        };

        // setMessages([{'local': true, 'message': 'Добрый деdshjfjes ddfgsg hfjehsdefjk hesdkjghfesjkdg fghkнь'}, {
        //     'local': true, 'message': 'Вы' +
        //         ' здесь?'
        // },
        //     {'local': false, 'message': 'Вас не слышно Вас не слышно Вас не слышно Вас не слышно Вас не слышно Вас не слышно'}, {'local': false, 'message': 'Включите' +
        //             ' микрофон'}, {'local': true, 'message': 'А так?'},
        //     {'local': true, 'message': 'Супер'}])

    }, [])

    const handleChange = (event) => {
        setCurrentMessage(event.target.value);
    };

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            sendMessage();
        }
    }

    return (
        <Grid container>
            <Grid className={'chat'} item xs={12}>
                <Paper className={'paper'}>
                    <Grid container direction="column" justify={'flex-end'}>
                        {messages && getMessages()}
                    </Grid>
                    <Grid item xs={12}>
                        <Input multiline={true} rowsMin={10} fullWidth value={currentMessage} onChange={handleChange}
                               onKeyDown={handleKeyDown}
                               endAdornment={
                                   <InputAdornment position="end">
                                       <IconButton onClick={sendMessage}>
                                           <Send/>
                                       </IconButton>
                                   </InputAdornment>
                               }
                        >
                        </Input>
                        {/*<Button variant="contained" color="secondary" onClick={sendMessage}>Отправить</Button>*/}
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    )
}