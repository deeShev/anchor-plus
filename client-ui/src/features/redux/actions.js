import {createAsyncThunk} from "@reduxjs/toolkit";
import {
    doLogin,
    doScheduleMeeting,
    getName,
    getProducts,
    doSubmit,
    getMeetings,
    doSaveMeeting
} from "../../service/Service";

export const fetchClientName = createAsyncThunk(
    'client/name',
    () => getName().then(response => {
        return response.data
    })
);

export const fetchProducts = createAsyncThunk(
    'core/products',
    () => getProducts().then(response => {
        return response.data
    })
);

export const login = createAsyncThunk(
    'client/login',
    (name) => doLogin(name).then(response => {
        return response.data
    })
);

export const scheduleMeeting = createAsyncThunk(
    'core/schedule_Meeting',
    (params) => {
        let {clientId, productId, dateTime} = params;
        return doScheduleMeeting(clientId, productId, dateTime)
            .then(response => {
                return response.data
            })
    }
);

export const submit = createAsyncThunk(
    'core/submit',
    (params) => {
        let {rank, comment, meetingId} = params;
        return doSubmit(rank, comment, meetingId)
            .then(response => {
                return response.data
            })
    }
);

export const fetchMeetings = createAsyncThunk(
    'core/fetchMeetings',
    (clientId) => {
        return getMeetings(clientId)
            .then(response => {
                return response.data
            })
    }
);

export const saveMeeting = createAsyncThunk(
    'core/fetchMeetings',
    (params) => {
        const {clientId, productId, employeeId, dateTime, comment} = params;
        return doSaveMeeting(clientId, productId, employeeId, dateTime, comment)
            .then(response => {
                return response.data
            })
    }
);

