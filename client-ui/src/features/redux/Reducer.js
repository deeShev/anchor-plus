import {createSlice} from '@reduxjs/toolkit';
import {fetchProducts, login, scheduleMeeting, submit, fetchMeetings} from "./actions";
import React from "react";
import * as R from 'ramda'

export const mainSlice = createSlice({
    name: 'main',
    initialState: {
        products: [],
        physProductsByCategory: {},
        corProductsByCategory: {},
        user: {},
        product: {},
        isMeetingAvailable: null,
        isMeetingComplete: false,
        meeting:{},
        meetings:[]
    },
    reducers: {
        setName: (state, action) => {
            state.text = action.payload;
        },
        setProduct: (state, action) => {
            state.product = action.payload;
        },
        logout: (state, action) => {
            state.user = {};
        },
        endMeeting: (state, action) => {
            state.isMeetingAvailable = null;
            state.isMeetingComplete = true;
        }
    },
    extraReducers: {
        [submit.fulfilled]: (state, action) => {
            state.isMeetingComplete = false;
            state.meeting = {};
        },
        [submit.rejected]: (state, action) => {
            state.isMeetingComplete = false;
            state.meeting = {};
        },
        [fetchMeetings.fulfilled]: (state, action) => {
           state.meetings = action.payload;
        },
        [fetchMeetings.rejected]: (state, action) => {
            state.meetings = [];
        },
        [scheduleMeeting.fulfilled]: (state, action) => {
            if (action.payload){
                state.meeting = action.payload;
                state.isMeetingAvailable = true;
            } else {
                state.isMeetingAvailable = false;
                state.meeting = {};
            }
        },
        [scheduleMeeting.rejected]: (state, action) => {
                state.isMeetingAvailable = false;
                state.meeting = {};
        },
        [scheduleMeeting.pending]: (state, action) => {
            state.isMeetingAvailable = null;
            state.meeting = {};
        },
        [login.fulfilled]: (state, action) => {
            state.user = action.payload;
        },
        [fetchProducts.fulfilled]: (state, action) => {
            console.log('lading products FULFILLED');

            let products = action.payload;
            let byCategoryFunc = R.groupBy((p) => p.category);
            let byPhysCorp = R.groupBy((p) => p.isPhys)(products) || {true: [], false: []};

            state.physProductsByCategory = byCategoryFunc(byPhysCorp[true]);
            state.corProductsByCategory = byCategoryFunc(byPhysCorp[false]);
            state.products = action.payload;
        },
        [fetchProducts.pending]: (state, action) => {
            console.log('lading products');
        },
        [fetchProducts.rejected]: (state, action) => {
            console.log('lading products REJECTED');
            state.products = [];
        }
    }
});

export const {setName, logout, setProduct, endMeeting} = mainSlice.actions;

export default mainSlice.reducer;
