import React from 'react';
import './App.css';
import Main from "./Main";
import Login from "./Login";
import {connect} from "react-redux";

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Booking from "./Meeting/booking";

export class App extends React.Component {

    render() {
        return <Router>
            <div className="App">
                <Switch>
                    <Route path={'/client-ui'} strict component={Main}/>
                    <Route path={'/client-login'} strict component={Login}/>
                    <Route path={'/booking'} strict component={Booking}/>
                </Switch>
            </div>
        </Router>
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        text: state.main.text
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(App)