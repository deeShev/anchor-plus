import React, {useEffect, useState} from "react"
import mainLogo from '../img/sberIdLogo.png';
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import {Link} from 'react-router-dom';
import Button from "@material-ui/core/Button";
import Input from "@material-ui/core/Input";
import {Checkbox} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import withStyles from "@material-ui/core/styles/withStyles";
import {login} from "../features/redux/actions";
import {connect} from "react-redux";
import MeetingCard from "../calendar/card";
import CreateDialog from "../calendar/createDialog";

const GreenCheckbox = withStyles({
    root: {
        color: '#19bb4f',
        '&$checked': {
            color: '#19bb4f',
        },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

const SberIdHeader = (props) => {
    useEffect(() => {
        if (props.user.login) {
            props.history.push("/booking");
        }
    });

    const [login, setLogin] = useState();

    return <div>
        <AppBar position="fixed" className={'app-bar'}>
            <Grid container style={{justifyContent: 'center'}}>
                <Grid item xl={4} xs={4}/>
                <Grid item xl={4} xs={4}>
                    <Toolbar>
                        <img src={mainLogo} className={'clickable'} style={{width: "105px", margin: "15px"}}
                             alt="fireSpot"/>
                        <div style={{flex: '1 0', display: 'flex', justifyContent: 'flex-end', alignItems: 'center'}}>
                            <Link to={'#'} className={"link"}>
                                <Typography variant="body1" noWrap style={{padding: '0px 20px'}}
                                            className={'clickable'}>
                                    Что такое Сбер ID? >
                                </Typography>
                            </Link>
                        </div>
                    </Toolbar>
                </Grid>
                <Grid item xl={4} xs={4}/>
            </Grid>
        </AppBar>
        <div style={{paddingTop: '64px'}}>
            <Grid container style={{justifyContent: 'center'}}>
                <Grid item xl={4} xs={4}/>
                <Grid item xl={4} xs={4}>
                    <h2 className="sberId1">Вход в «id.sber.ru»</h2>
                    <Grid container style={{justifyContent: 'center'}}>
                        <Grid item xl={4} xs={4}>
                            <Button className={"sberId2"}>QR-код</Button>
                        </Grid>
                        <Grid item xl={4} xs={4}>
                            <Button className={"sberId2"}>Биометрия</Button>
                        </Grid>
                        <Grid item xl={4} xs={4}>
                            <Button className={"sberId2dark"}>Логин</Button>
                        </Grid>
                    </Grid>
                    <br/>
                    <Grid container style={{justifyContent: 'center'}}>
                        <Input disableUnderline placeholder={" Логин"}
                               onChange={(e) => setLogin(e.target.value)}
                               className={"sberId3"} type={"login"}/>
                    </Grid>
                    <br/>
                    <Grid container style={{justifyContent: 'center'}}>
                        <Input disableUnderline placeholder={" Пароль"} className={"sberId3"} type={"password"}/>
                    </Grid>
                    <br/>
                    <Grid container style={{justifyContent: 'center'}}>
                        <Grid item xl={6} xs={6}>
                            <Button className={"sberId4"} onClick={() => props.doLogin(login)}>Войти</Button>
                        </Grid>
                        <Grid item xl={6} xs={6}>
                            <FormControlLabel
                                control={<GreenCheckbox checked/>}
                                label={"Запомнить мой Сбер ID"}/>
                        </Grid>
                    </Grid>
                    <br/>
                    <hr/>
                    <Grid container style={{justifyContent: 'center'}}>

                        <Grid item xl={6} xs={6}>
                            <div style={{flex: '1 0', display: 'flex', alignItems: 'center'}}>
                                < Link to={'#'} className={"link"}>
                                    <Typography variant="body1" noWrap style={{padding: '0px 20px'}}
                                                className={'clickable'}>
                                        Создать Сбер ID? >
                                    </Typography>
                                </Link>
                            </div>
                        </Grid>
                        <Grid item xl={6} xs={6}>
                            <span style={{color: "#999", fontSize: "12px", fontFamily: "sans-serif"}}>Сбер ID - единый вход в сервисы Сбера</span>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xl={4} xs={4}>
                    {/*<MeetingCard meeting={{dateTime: 1601428382576, employee: {firstName: "Анна", lastName: 'Петрова'}, product:{name: "СберЗдоровье"}}}/>*/}
                    {/*<CreateDialog product={{name: "DeliveryClub"}}/>*/}
                </Grid>
            </Grid>
        </div>
    </div>
};

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.main.user
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doLogin: (name) => {
            dispatch(login(name));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SberIdHeader);