import React from "react"
import mainLogo from '../img/sber_logo_main.png';
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import {AiOutlineSearch, TiLocationArrow} from "react-icons/all";
import Grid from "@material-ui/core/Grid";
import {Link} from 'react-router-dom';


const Header = (props) => {
    return <div>
        <AppBar position="fixed" className={'app-bar'}>
            <Grid container style={{justifyContent: 'center'}}>
                <Grid item xl={8} xs={10}>
                    <Toolbar>
                        <div
                            color="inherit"
                            aria-label="open drawer"
                            onClick={() => {
                            }}
                            className={'header-menu'}/>
                        <img src={mainLogo} className={'clickable'} style={{width: "105px", margin: "15px"}}
                             alt="fireSpot"/>

                        <Link to="/client-ui/for-me" className={'link'}>
                            <Typography variant="body1" noWrap style={{padding: '0px 20px'}} className={'clickable'}>
                                Для меня
                            </Typography>
                        </Link>
                        <Link to="/client-ui/for-business" className={'link'}>
                            <Typography variant="body1" noWrap style={{padding: '0px 20px'}} className={'clickable'}>
                                Для бизнеса
                            </Typography>
                        </Link>

                        <div style={{flex: '1 0', display: 'flex', justifyContent: 'flex-end', alignItems: 'center'}}>
                            <TiLocationArrow className={'icon'}/>
                            <Typography variant="body1">
                                Санкт-Петербург
                            </Typography>
                        </div>
                        <AiOutlineSearch className={'icon px10'}/>
                    </Toolbar>
                </Grid>
            </Grid>
        </AppBar>
    </div>
};

export default Header;