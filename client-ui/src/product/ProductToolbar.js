import React from "react"
import Toolbar from "@material-ui/core/Toolbar";
import AnchorLink from "react-anchor-link-smooth-scroll";


const ProductToolbar = (props) => {
    return <div>
        <Toolbar>
            {Object.keys(props.products || {})
                .map((p, i) => <AnchorLink key={p} href={'#' + p} className={'anchor'}>{p}</AnchorLink>)}
        </Toolbar>
    </div>
};

export default ProductToolbar;
