import React from "react"
import Typography from "@material-ui/core/Typography";
import VideocamIcon from "@material-ui/icons/Videocam"
import {connect} from "react-redux";
import {fetchProducts} from "../features/redux/actions";
import {setProduct} from "../features/redux/Reducer";

const Product = (props) => {
    return <div className={'pg-25'}>
        <div className={'flex-center'}>
            <div style={{height: '60px', width: '60px', borderRadius: '4px'}}>
                <div style={{
                    height: '60px', width: '60px',
                    boxShadow: '0 0 8px 0 rgba(0,0,0,.08)',
                    backgroundSize: 'contain', backgroundImage: 'url(' + window.atob(props.product.iconUrl) + ')'
                }}/>
            </div>

            <div className={'px10'}>
                <Typography variant={'h6'} style={{flex: '1 0'}}>{props.product.name}</Typography>
            </div>

        </div>
        <Typography align={"left"} variant={'subtitle1'} style={{width: '100%', paddingTop: '20px'}}>
            {props.product.description}
        </Typography>
        <div>
            <Typography align={"left"} variant={'subtitle2'} style={{width: '100%', paddingTop: '20px'}}>
                <a href={props.product.websiteUrl} className={'link-light'}> Перейти на сайт</a>
            </Typography>
            <div className={'flex-center'} onClick={() => toBooking(props)}>
                <div><Typography align={"left"} variant={'subtitle2'} style={{width: '100%'}}>
                    <a href={props.product.websiteUrl} className={'link-light'}> Консультация</a>
                </Typography>
                </div>
                <VideocamIcon style={{fill: "#19bb4f", paddingLeft: '5px'}}/>
            </div>
        </div>
    </div>
};

const toBooking = (ownProps) => {
    ownProps.history.push((!ownProps.user.login) ? "/client-login" : "/booking");
    ownProps.choseProduct(ownProps.product);
};

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.main.user
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        choseProduct: (p) => {
            dispatch(setProduct(p));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
