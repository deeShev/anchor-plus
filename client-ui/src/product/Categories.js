import React from "react"
import Category from "./Category";

const Categories = (props) => {

    return <div>
        {Object.keys(props.products).map(cat => {
            return <Category key={cat} name={cat} products={props.products[cat]}
                             history = {props.history}/>
        })}
    </div>
};


export default Categories;