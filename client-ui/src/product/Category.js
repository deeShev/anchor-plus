import React from "react"
import Product from "./Product";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

const Category = (props) => {

    return <div className={'pgy30'}>
        <Typography variant={'h4'}>{props.name}</Typography>
        <Grid container style={{paddingTop: '30px'}}>
        {(props.products || []).map(p => {
            return <Grid item key={p.id} xs={12} sm={4} xl={3}>
                <Product  product={p} history = {props.history}/>
            </Grid>
        })}
        </Grid>
    </div>
};

export default Category;