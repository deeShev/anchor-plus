import {configureStore} from '@reduxjs/toolkit';
import mainReducer from '../features/redux/Reducer';
import logger from 'redux-logger'
import {applyMiddleware} from "redux";

export default configureStore({
    reducer: {
        main: mainReducer,
    },
    enhancers: [
        applyMiddleware(logger)]
});
