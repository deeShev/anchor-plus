import React from "react"
import {connect} from 'react-redux'
import {setName} from "../features/redux/Reducer"
import {fetchClientName} from "../features/redux/actions";
import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField";


const Info = (props) => {
    return <div>
        <div>Info</div>
        <TextField id="standard-basic" label="Flight name" value={props.text || ''}
                   onChange={props.onChange}/>
        <Button variant="contained" onClick={props.getName}>Fetch Name</Button>
        <div><Button variant="contained" onClick={()=> {
            props.history.push('/client-ui');
        }}>back_to_main</Button></div>
        <div>{props.text}</div>
    </div>;
};

const mapStateToProps = (state, ownProps) => {
    return {
        text: state.main.text
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getName: () => {
            dispatch(fetchClientName());
        },
        onChange: (e) => {
            dispatch(setName(e.target.value));
            console.log(e.target.value);
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Info)

