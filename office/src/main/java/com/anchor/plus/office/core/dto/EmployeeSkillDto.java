package com.anchor.plus.office.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class EmployeeSkillDto implements Serializable {
    private static final long serialVersionUID = 3679518541965611450L;

    private EmployeeDto employee;
    private Set<SkillDto> skillDtos;
}
