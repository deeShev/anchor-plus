package com.anchor.plus.office.signaling;

/**
 * @author Guillaume Gerbaud
 */
public class SignalMessage {

    private String event;
    private Integer meeting;
    private Object data;

    public SignalMessage() {

    }

    public SignalMessage(String event) {
        this.event = event;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Integer getMeeting() {
        return meeting;
    }

    public void setMeeting(Integer meeting) {
        this.meeting = meeting;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}