package com.anchor.plus.office.signaling;

import com.anchor.plus.office.core.dto.MeetingJoinDto;
import com.anchor.plus.office.core.service.MeetingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SocketHandler extends TextWebSocketHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SocketHandler.class);

    private static final String LOGIN_TYPE = "login";

    private MeetingService meetingService;

    private ObjectMapper objectMapper = new ObjectMapper();

    // username-session
    private Map<Integer, WebSocketSession> clients = new ConcurrentHashMap<>();
    // sessionid-username
    private Map<String, Integer> clientIds = new ConcurrentHashMap<>();

    private Map<WebSocketSession, WebSocketSession> connected = new ConcurrentHashMap<>();

    @Autowired
    public SocketHandler(MeetingService meetingService) {
        this.meetingService = meetingService;
    }


    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

        SignalMessage signalMessage = objectMapper.readValue(message.getPayload(), SignalMessage.class);

        if (LOGIN_TYPE.equalsIgnoreCase(signalMessage.getEvent())) {

            // It's a login message so we assume data to be a String representing the userId
            Integer userId = (Integer) signalMessage.getData();
            LOGGER.info("Start to login user {} meeting {}", userId, signalMessage.getMeeting());

            WebSocketSession client = clients.get(userId);

            // quick check to verify that the userId is not already taken and active
            if (client == null || !client.isOpen()) {
                // saves socket and userId
                clients.put(userId, session);
                clientIds.put(session.getId(), userId);
            } else {
                throw new SocketException(String.format("User %s already registered", userId));
            }

            MeetingJoinDto meeting = meetingService.getMeetingById(signalMessage.getMeeting());
            if (meeting == null) {
                throw new SocketException(String.format("Cannot find meeting %s", meeting));
            }

            Integer companion;
            if (meeting.getClient().getId().equals(userId)) {
                companion = meeting.getEmployee().getId();
            } else {
                companion = meeting.getClient().getId();
            }

            if (clients.get(companion) != null) {
                WebSocketSession companionSession = clients.get(companion);
                connected.put(session, companionSession);
                SignalMessage readyMessage = new SignalMessage("ready");
                companionSession.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyMessage)));
                LOGGER.info("clients after login {}", clients);
                LOGGER.info("connecter after login {}", connected);
            }

        } else {
            WebSocketSession destSession = connected.get(session);
            if (destSession == null) {
                destSession = connected.entrySet().stream()
                        .filter(entry -> entry.getValue().equals(session))
                        .map(Map.Entry::getKey)
                        .findFirst()
                        .orElse(null);
            }
            if (destSession == null || !destSession.isOpen()) {
                LOGGER.error("user {} you do not have connected user", clientIds.get(session.getId()));
                return;
            }

            destSession.sendMessage(message);

        }

    }


    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        LOGGER.warn("connection closed");
        super.afterConnectionClosed(session, status);
        clientIds.remove(session.getId());
        clients.entrySet().removeIf(entry -> entry.getValue().equals(session));
        connected.remove(session);
        connected.entrySet().removeIf(entry -> entry.getValue().equals(session));
        LOGGER.info("clientIds after remove {}", clientIds);
        LOGGER.info("clients after remove {}", clients);

    }
}