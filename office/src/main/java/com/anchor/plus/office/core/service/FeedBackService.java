package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.FeedBackDto;

public interface FeedBackService {
    FeedBackDto save(FeedBackDto feedBackDto);
}
