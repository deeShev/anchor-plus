package com.anchor.plus.office.core.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "meeting")
@EqualsAndHashCode(callSuper = true)
public class Meeting extends DomainBase {
    @Column(name = "client_id")
    private Integer clientId;
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "employee_id")
    private Integer employeeId;
    private Long dateTime;
    private String comment;
    private String recordUrl;
    private String clientNotes;
    private String employeeNotes;
    private Long bTime;
    private Long eTime;
}
