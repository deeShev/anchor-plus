package com.anchor.plus.office.core.repository;

import com.anchor.plus.office.core.entities.MeetingJoin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeetingJoinRepository extends CrudRepository<MeetingJoin, Integer> {
    List<MeetingJoin> findAllByClientId(Integer clientId);

    List<MeetingJoin> findAllByEmployeeId(Integer employeeId);
}
