package com.anchor.plus.office.core.mapper;

import com.anchor.plus.office.core.dto.EmployeeDto;
import com.anchor.plus.office.core.entities.Employee;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR, componentModel = "spring")
public interface EmployeeMapper extends BaseMapper<Employee, EmployeeDto> {
}