package com.anchor.plus.office.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class ProductDto implements Serializable {
    private static final long serialVersionUID = 7219048849555213214L;

    private Integer id;
    private String name;
    private String description;
    private String websiteUrl;
    private byte[] iconUrl;
    private String category;
    private Boolean isCorp;
    private Boolean isPhys;
}
