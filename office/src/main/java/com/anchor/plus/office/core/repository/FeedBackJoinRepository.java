package com.anchor.plus.office.core.repository;

import com.anchor.plus.office.core.entities.FeedBackJoin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeedBackJoinRepository extends CrudRepository<FeedBackJoin, Integer> {
    List<FeedBackJoin> findAllByMeetingIdIn(List<Integer> meetingIds);
}
