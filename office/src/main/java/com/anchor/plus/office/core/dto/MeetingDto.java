package com.anchor.plus.office.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor(force = true)
@Builder(toBuilder = true)
@AllArgsConstructor
public class MeetingDto implements Serializable {
    private static final long serialVersionUID = 2703208469468010511L;

    private Integer id;
    private Integer clientId;
    private Integer productId;
    private Integer employeeId;
    private Long dateTime;
    private String comment;
    private String recordUrl;
    private String clientNotes;
    private String employeeNotes;
    private Long bTime;
    private Long eTime;
}
