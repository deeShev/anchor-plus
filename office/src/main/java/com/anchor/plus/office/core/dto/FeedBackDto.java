package com.anchor.plus.office.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class FeedBackDto implements Serializable {
    private static final long serialVersionUID = 444572022483853572L;

    private Integer id;
    private Integer meetingId;
    private Integer csi;
    private String comment;
}
