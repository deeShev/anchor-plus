package com.anchor.plus.office.core.repository;

import com.anchor.plus.office.core.entities.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Integer> {
    Client findByLogin(String login);
}
