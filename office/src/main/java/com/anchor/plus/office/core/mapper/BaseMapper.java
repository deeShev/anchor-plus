package com.anchor.plus.office.core.mapper;

public interface BaseMapper<E, D> {
    E entity(D dto);

    D dto(E entity);
}
