package com.anchor.plus.office.core.mapper;

import com.anchor.plus.office.core.dto.ProductDto;
import com.anchor.plus.office.core.entities.Product;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {MapperUtils.class}, injectionStrategy = InjectionStrategy.CONSTRUCTOR, componentModel = "spring")
public interface ProductMapper extends BaseMapper<Product, ProductDto> {
    @Mapping(target = "isCorp", source = "isCorp", qualifiedByName = "getIntOfBoolean")
    @Mapping(target = "isPhys", source = "isPhys", qualifiedByName = "getIntOfBoolean")
    @Override
    Product entity(ProductDto dto);

    @Mapping(target = "isCorp", source = "isCorp", qualifiedByName = "getBooleanOfInt")
    @Mapping(target = "isPhys", source = "isPhys", qualifiedByName = "getBooleanOfInt")
    @Override
    ProductDto dto(Product entity);
}
