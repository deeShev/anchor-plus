package com.anchor.plus.office.operator.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OperatorController {

    @RequestMapping("/operator-ui")
    String home() {
        return "operator-index.html";
    }
}
