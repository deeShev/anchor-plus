package com.anchor.plus.office;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.anchor.plus.office")
public class ServerApplicationStarter {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplicationStarter.class, args);
    }

}
