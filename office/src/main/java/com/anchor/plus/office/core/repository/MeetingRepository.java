package com.anchor.plus.office.core.repository;

import com.anchor.plus.office.core.entities.Meeting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeetingRepository extends CrudRepository<Meeting, Integer> {
}
