package com.anchor.plus.office.core.repository;

import com.anchor.plus.office.core.entities.FeedBack;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedBackRepository extends CrudRepository<FeedBack, Integer> {
}
