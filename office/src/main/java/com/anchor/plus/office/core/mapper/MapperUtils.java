package com.anchor.plus.office.core.mapper;

import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class MapperUtils {
    @Named("getBooleanOfInt")
    public Boolean getBooleanOfInt(Integer value) {
        return Objects.isNull(value) ? Boolean.FALSE : (value == 0 ? Boolean.FALSE : Boolean.TRUE);
    }

    @Named("getIntOfBoolean")
    public Integer getIntOfBoolean(Boolean value) {
        return Objects.isNull(value) ? 0 : (value.equals(Boolean.FALSE) ? 0 : 1);
    }
}
