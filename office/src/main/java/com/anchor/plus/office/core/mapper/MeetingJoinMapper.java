package com.anchor.plus.office.core.mapper;

import com.anchor.plus.office.core.dto.MeetingJoinDto;
import com.anchor.plus.office.core.entities.MeetingJoin;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(uses = {ClientMapper.class, EmployeeMapper.class, ProductMapper.class},
        injectionStrategy = InjectionStrategy.CONSTRUCTOR, componentModel = "spring")
public interface MeetingJoinMapper extends BaseMapper<MeetingJoin, MeetingJoinDto> {
}
