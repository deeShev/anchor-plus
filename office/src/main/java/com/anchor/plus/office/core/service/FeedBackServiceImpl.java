package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.FeedBackDto;
import com.anchor.plus.office.core.mapper.FeedBackMapper;
import com.anchor.plus.office.core.repository.FeedBackRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class FeedBackServiceImpl implements FeedBackService {
    private final FeedBackRepository repository;
    private final FeedBackMapper mapper;

    @Override
    public FeedBackDto save(FeedBackDto feedBackDto) {
        if (isNull(feedBackDto)) return null;
        return mapper.dto(repository.save(mapper.entity(feedBackDto)));
    }
}
