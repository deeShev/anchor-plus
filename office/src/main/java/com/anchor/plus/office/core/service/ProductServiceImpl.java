package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.ProductDto;
import com.anchor.plus.office.core.entities.Product;
import com.anchor.plus.office.core.mapper.BaseMapper;
import com.anchor.plus.office.core.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final BaseMapper<Product, ProductDto> mapper;
    private final ProductRepository repository;

    @Override
    public List<ProductDto> findAll() {
        return repository.findAll().stream()
                .map(mapper::dto)
                .collect(toList());
    }
}
