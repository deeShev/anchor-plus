package com.anchor.plus.office.core.repository;

import com.anchor.plus.office.core.entities.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    @Query(value = "SELECT e FROM Employee e left join Skill s on e.id = s.employee.id " +
            "WHERE e.status = :status AND s.product.id = :productId")
    List<Employee> findAllByStatus(@Param("status") Integer status, @Param("productId") Integer productId);

    List<Employee> findAllByStatus(Integer status);

    List<Employee> findAll();
}
