package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.ClientDto;

public interface ClientService {
    ClientDto getByLogin(String login);
}
