package com.anchor.plus.office.client.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ClientsController {

    @RequestMapping("/client-ui")
    String home() {
        return "client-index.html";
    }
}
