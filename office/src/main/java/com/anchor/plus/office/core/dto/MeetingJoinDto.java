package com.anchor.plus.office.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class MeetingJoinDto implements Serializable {
    private static final long serialVersionUID = 1266233348808306514L;

    private Integer id;
    private ClientDto client;
    private ProductDto product;
    private EmployeeDto employee;
    private FeedBackDto feedBack;
    private Long dateTime;
    private String comment;
    private String recordUrl;
    private String clientNotes;
    private String employeeNotes;
    private Long bTime;
    private Long eTime;
}
