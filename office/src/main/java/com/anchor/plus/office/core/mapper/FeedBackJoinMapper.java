package com.anchor.plus.office.core.mapper;

import com.anchor.plus.office.core.dto.FeedBackDto;
import com.anchor.plus.office.core.entities.FeedBackJoin;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR, componentModel = "spring")
public interface FeedBackJoinMapper extends BaseMapper<FeedBackJoin, FeedBackDto> {
}
