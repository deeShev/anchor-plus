package com.anchor.plus.office.core.mapper;

import com.anchor.plus.office.core.dto.SkillDto;
import com.anchor.plus.office.core.entities.Skill;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(uses = ProductMapper.class, injectionStrategy = InjectionStrategy.CONSTRUCTOR, componentModel = "spring")
public interface SkillMapper extends BaseMapper<Skill, SkillDto> {
}
