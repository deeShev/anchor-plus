package com.anchor.plus.office.core.mapper;

import com.anchor.plus.office.core.dto.FeedBackDto;
import com.anchor.plus.office.core.entities.FeedBack;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR, componentModel = "spring")
public interface FeedBackMapper extends BaseMapper<FeedBack, FeedBackDto> {
}
