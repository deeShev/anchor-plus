package com.anchor.plus.office.core.service;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.function.Supplier;

@UtilityClass
public class ServiceUtils {
    public static <T> T safe(Supplier<T> function) {
        try {
            return function.get();
        } catch (Exception e) {
            return null;
        }
    }

    public static Integer getStatusByDateTime(Long dateTime) {
        Date currentDateMinusHours = Date.from(LocalDateTime.now().minusHours(1).atZone(ZoneId.systemDefault()).toInstant());
        Date currentDate = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        if (currentDateMinusHours.equals(new Date(dateTime)) ||
                (currentDateMinusHours.before(new Date(dateTime)) && currentDate.after(new Date(dateTime)))) {
            return 1;
        }
        return 0;
    }
}