package com.anchor.plus.office.core.mapper;

import com.anchor.plus.office.core.dto.ClientDto;
import com.anchor.plus.office.core.entities.Client;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR, componentModel = "spring")
public interface ClientMapper extends BaseMapper<Client, ClientDto> {
}
