package com.anchor.plus.office.core.repository;

import com.anchor.plus.office.core.entities.Skill;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SkillRepository extends CrudRepository<Skill, Integer> {
    List<Skill> findAllByEmployeeId(Integer id);
}
