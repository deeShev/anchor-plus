package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.EmployeeDto;
import com.anchor.plus.office.core.dto.EmployeeSkillDto;
import com.anchor.plus.office.core.dto.SkillDto;
import com.anchor.plus.office.core.entities.Skill;
import com.anchor.plus.office.core.mapper.EmployeeMapper;
import com.anchor.plus.office.core.mapper.SkillMapper;
import com.anchor.plus.office.core.repository.SkillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SkillServiceImpl implements SkillService {
    private final SkillMapper skillMapper;
    private final SkillRepository skillRepository;
    private final EmployeeMapper employeeMapper;

    @Override
    public EmployeeSkillDto getSkillsByEmployeeId(Integer id) {
        List<Skill> skills = skillRepository.findAllByEmployeeId(id);
        if (CollectionUtils.isEmpty(skills)) return null;
        Set<SkillDto> skillDtos = skills.stream()
                .map(skillMapper::dto)
                .collect(Collectors.toSet());

        EmployeeDto employeeDto = skills.stream()
                .map(Skill::getEmployee)
                .map(employeeMapper::dto)
                .findFirst()
                .orElse(null);

        return EmployeeSkillDto.builder()
                .employee(employeeDto)
                .skillDtos(skillDtos)
                .build();
    }
}
