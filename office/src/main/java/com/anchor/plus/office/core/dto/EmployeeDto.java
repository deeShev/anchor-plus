package com.anchor.plus.office.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class EmployeeDto implements Serializable {
    private static final long serialVersionUID = -8387419550472781332L;

    private Integer id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Integer status;
}
