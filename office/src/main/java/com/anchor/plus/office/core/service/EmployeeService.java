package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.EmployeeDto;

public interface EmployeeService {
    EmployeeDto getByStatus(Long dateTime, Integer productId);

    EmployeeDto updateStatus(Integer id, Integer status);
}
