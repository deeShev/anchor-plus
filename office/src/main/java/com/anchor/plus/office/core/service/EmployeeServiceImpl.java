package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.EmployeeDto;
import com.anchor.plus.office.core.entities.Employee;
import com.anchor.plus.office.core.mapper.EmployeeMapper;
import com.anchor.plus.office.core.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.SplittableRandom;

import static java.util.Objects.isNull;
import static org.springframework.util.CollectionUtils.isEmpty;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository repository;
    private final EmployeeMapper mapper;

    @Override
    public EmployeeDto getByStatus(Long dateTime, Integer productId) {
        Integer status = ServiceUtils.getStatusByDateTime(dateTime);
        List<Employee> employees;

        if (new Integer(0).equals(status)) {
            employees = repository.findAll();
        }else {
            employees = repository.findAllByStatus(status, productId);
        }

        if (isEmpty(employees)) {
            return null;
        }

        int randomValue =  new SplittableRandom().nextInt(0, employees.size());
        return mapper.dto(employees.get(randomValue));
    }

    @Override
    public EmployeeDto updateStatus(Integer id, Integer status) {
        if (isNull(id)) return null;
        Employee employee = repository.findById(id).orElse(null);
        if (isNull(employee)) {
            return null;
        } else {
            Employee build = employee.toBuilder()
                    .status(status)
                    .build();
            Employee save = repository.save(build);
            return mapper.dto(save);
        }
    }
}