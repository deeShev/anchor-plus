package com.anchor.plus.office.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class ClientDto implements Serializable {
    private static final long serialVersionUID = 355394834056795148L;

    private Integer id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String login;
}
