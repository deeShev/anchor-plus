package com.anchor.plus.office.core.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "feedback")
@EqualsAndHashCode(callSuper = true)
public class FeedBack extends DomainBase {
    @Column(name = "meeting_id")
    private Integer meetingId;
    private Integer csi;
    private String comment;
}
