package com.anchor.plus.office.core.mapper;

import com.anchor.plus.office.core.dto.MeetingDto;
import com.anchor.plus.office.core.entities.Meeting;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR, componentModel = "spring")
public interface MeetingMapper extends BaseMapper<Meeting, MeetingDto> {
}
