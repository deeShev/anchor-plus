package com.anchor.plus.office.client.controllers;

import com.anchor.plus.office.core.dto.*;
import com.anchor.plus.office.core.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("core")
@RequiredArgsConstructor
public class CoreRestController {
    private final ProductService productService;
    private final MeetingService meetingService;
    private final ClientService clientService;
    private final FeedBackService feedBackService;
    private final EmployeeService employeeService;
    private final SkillService skillService;

    @GetMapping("products")
    public List<ProductDto> getProducts() {
        return productService.findAll();
    }

    @GetMapping("meeting-by-employee-id")
    public List<MeetingJoinDto> getMeetingJoinDtoByEmployeeId(@RequestParam Integer id) {
        return meetingService.getMeetingByEmployeeId(id);
    }

    @GetMapping("meeting-by-client-id")
    public List<MeetingJoinDto> getMeetingJoinDtoByClientId(@RequestParam Integer id) {
        return meetingService.getMeetingByClientId(id);
    }

    @GetMapping("meeting-by-id")
    public MeetingJoinDto getMeetingJoinDtoById(@RequestParam Integer id) {
        return meetingService.getMeetingById(id);
    }

    @PostMapping("meeting-save-dto")
    public MeetingDto saveMeeting(@RequestBody MeetingDto meetingDto) {
        return meetingService.save(meetingDto);
    }

    @PostMapping("meeting-save-dto-random")
    public MeetingDto saveMeetingByRandomEmpl(@RequestBody MeetingDto meetingDto) {
        EmployeeDto employeeDto = employeeService.getByStatus(meetingDto.getDateTime(),
                meetingDto.getProductId());
        if (Objects.isNull(employeeDto)) return null;
        return meetingService.save(meetingDto.toBuilder().employeeId(employeeDto.getId()).build());
    }

    @GetMapping("client-by-login")
    public ClientDto getClientByLogin(@RequestParam String login) {
        return clientService.getByLogin(login);
    }

    @PostMapping("feedback-save")
    public FeedBackDto save(@RequestBody FeedBackDto feedBackDto) {
        return feedBackService.save(feedBackDto);
    }

    @GetMapping("meeting-get-random-empl")
    public MeetingJoinDto getMeetingJoinByRandomEmpl(@RequestParam Integer clientId,
                                                     @RequestParam Integer productId,
                                                     @RequestParam Long dateTime) {
        //1- free
        EmployeeDto employeeDto = employeeService.getByStatus(dateTime, productId);
        if (Objects.isNull(employeeDto)) return null;
        MeetingDto meetingDto = MeetingDto.builder()
                .productId(productId)
                .clientId(clientId)
                .employeeId(employeeDto.getId())
                .dateTime(dateTime)
                .build();
        return meetingService.getMeetingById(meetingService.save(meetingDto).getId());
    }

    @GetMapping("skills-by-employee-id")
    public EmployeeSkillDto getSkillsByEmployeeId(@RequestParam Integer id) {
        return skillService.getSkillsByEmployeeId(id);
    }

    @GetMapping("employee-update-new-status")
    public EmployeeDto updateEmployeeWithNewStatus(@RequestParam Integer id, @RequestParam Integer status) {
        return employeeService.updateStatus(id, status);
    }

    @GetMapping("meeting-delete-by-id")
    public void deleteMeetingById(@RequestParam Integer id) {
        meetingService.delete(id);
    }
}