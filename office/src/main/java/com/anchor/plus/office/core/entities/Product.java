package com.anchor.plus.office.core.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product")
@EqualsAndHashCode(callSuper = true)
public class Product extends DomainBase {
    private String name;
    private String description;
    @Column(name = "website_url")
    private String websiteUrl;
    @Column(name = "icon_url")
    private byte[] iconUrl;
    private String category;
    @Column(name = "is_corp")
    private Integer isCorp;
    @Column(name = "is_phys")
    private Integer isPhys;
}