package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.MeetingDto;
import com.anchor.plus.office.core.dto.MeetingJoinDto;

import java.util.List;

public interface MeetingService {
    List<MeetingJoinDto> getMeetingByClientId(Integer id);

    List<MeetingJoinDto> getMeetingByEmployeeId(Integer id);

    MeetingJoinDto getMeetingById(Integer id);

    MeetingDto save(MeetingDto meetingDto);

    MeetingJoinDto save(MeetingJoinDto meetingDto);

    void delete(Integer id);
}