package com.anchor.plus.office.core.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "feedback")
@EqualsAndHashCode(callSuper = true)
public class FeedBackJoin extends DomainBase {
    @OneToOne
    @JoinColumn(name = "meeting_id")
    private MeetingJoin meeting;
    private Integer csi;
    private String comment;
}
