package com.anchor.plus.office.operator.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("operator")
public class OperatorRestController {

    @GetMapping("name")
    public String name() {
        return "Zorro";
    }
}
