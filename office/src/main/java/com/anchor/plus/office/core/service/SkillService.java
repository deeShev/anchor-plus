package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.EmployeeSkillDto;

public interface SkillService {
    EmployeeSkillDto getSkillsByEmployeeId(Integer id);
}
