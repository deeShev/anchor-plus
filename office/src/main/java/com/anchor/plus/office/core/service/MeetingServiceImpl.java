package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.MeetingDto;
import com.anchor.plus.office.core.dto.MeetingJoinDto;
import com.anchor.plus.office.core.entities.DomainBase;
import com.anchor.plus.office.core.entities.FeedBackJoin;
import com.anchor.plus.office.core.entities.Meeting;
import com.anchor.plus.office.core.entities.MeetingJoin;
import com.anchor.plus.office.core.mapper.FeedBackJoinMapper;
import com.anchor.plus.office.core.mapper.MeetingJoinMapper;
import com.anchor.plus.office.core.mapper.MeetingMapper;
import com.anchor.plus.office.core.repository.FeedBackJoinRepository;
import com.anchor.plus.office.core.repository.MeetingJoinRepository;
import com.anchor.plus.office.core.repository.MeetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static com.anchor.plus.office.core.service.ServiceUtils.safe;
import static java.lang.System.currentTimeMillis;
import static java.util.Collections.singletonList;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Service
@RequiredArgsConstructor
public class MeetingServiceImpl implements MeetingService {
    private final MeetingJoinRepository meetingJoinRepository;
    private final FeedBackJoinRepository feedBackRepository;
    private final MeetingJoinMapper meetingJoinMapper;
    private final FeedBackJoinMapper feedBackMapper;
    private final MeetingMapper meetingMapper;
    private final MeetingRepository meetingRepository;

    @Override
    public List<MeetingJoinDto> getMeetingByClientId(Integer id) {
        List<MeetingJoin> meetings = meetingJoinRepository.findAllByClientId(id);
        Map<Integer, FeedBackJoin> feedBacks = getFeedBacksByMeetingId(meetings);
        return meetings.stream()
                .map(meetingJoinMapper::dto)
                .filter(meetingJoinDto -> isActualDate(meetingJoinDto.getDateTime()))
                .peek(meetingJoinDto -> meetingJoinDto.setFeedBack(feedBackMapper.dto(feedBacks.get(meetingJoinDto.getId()))))
                .collect(toList());
    }

    @Override
    public List<MeetingJoinDto> getMeetingByEmployeeId(Integer id) {
        List<MeetingJoin> meetings = meetingJoinRepository.findAllByEmployeeId(id);
        Map<Integer, FeedBackJoin> feedBacks = getFeedBacksByMeetingId(meetings);
        return meetings.stream()
                .map(meetingJoinMapper::dto)
                .filter(meetingJoinDto -> isActualDate(meetingJoinDto.getDateTime()))
                .peek(meetingJoinDto -> meetingJoinDto.setFeedBack(feedBackMapper.dto(feedBacks.get(meetingJoinDto.getId()))))
                .collect(toList());
    }

    @Override
    public MeetingJoinDto getMeetingById(Integer id) {
        if (isNull(id)) return null;
        MeetingJoin meeting = meetingJoinRepository.findById(id).orElse(null);
        Map<Integer, FeedBackJoin> feedBacks = getFeedBacksByMeetingId(singletonList(meeting));
        MeetingJoinDto result = meetingJoinMapper.dto(meeting);
        if (isNull(result)) return null;
        result.setFeedBack(feedBackMapper.dto(feedBacks.get(Objects.requireNonNull(meeting).getId())));
        return result;
    }

    @Override
    public MeetingDto save(MeetingDto meetingDto) {
        Meeting entity = meetingMapper.entity(meetingDto);
        return meetingMapper.dto(safe(() -> meetingRepository.save(entity)));
    }

    @Override
    public MeetingJoinDto save(MeetingJoinDto meetingDto) {
        MeetingJoin entity = meetingJoinMapper.entity(meetingDto);
        return meetingJoinMapper.dto(safe(() -> meetingJoinRepository.save(entity)));
    }

    public void delete(Integer id) {
        meetingJoinRepository.deleteById(id);
    }

    private Map<Integer, FeedBackJoin> getFeedBacksByMeetingId(List<MeetingJoin> meetings) {
        return feedBackRepository.findAllByMeetingIdIn(meetings.stream()
                .filter(Objects::nonNull)
                .map(DomainBase::getId)
                .collect(toList())).stream()
                .collect(toMap(feedBack -> feedBack.getMeeting().getId(), Function.identity()));
    }

    private Boolean isActualDate(Long dateTime) {
        Date currentDateMinusHours = Date.from(LocalDateTime.now().minusHours(1).atZone(ZoneId.systemDefault()).toInstant());
        if (isNull(dateTime) || new Date(dateTime).equals(currentDateMinusHours) || new Date(dateTime).after(currentDateMinusHours)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}