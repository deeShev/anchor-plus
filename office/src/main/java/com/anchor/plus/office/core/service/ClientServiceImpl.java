package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.ClientDto;
import com.anchor.plus.office.core.entities.Client;
import com.anchor.plus.office.core.mapper.ClientMapper;
import com.anchor.plus.office.core.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;

    @Override
    public ClientDto getByLogin(String login) {
        Client client = clientRepository.findByLogin(login);
        if (isNull(client)) {
            client = Client.builder()
                    .firstName(login)
                    .lastName(login)
                    .middleName(login)
                    .login(login)
                    .build();
            client = clientRepository.save(client);
        }
        return clientMapper.dto(client);
    }
}
