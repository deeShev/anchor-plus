package com.anchor.plus.office.core.service;

import com.anchor.plus.office.core.dto.ProductDto;

import java.util.List;

public interface ProductService {
    List<ProductDto> findAll();
}
