package com.anchor.plus.office.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class SkillDto implements Serializable {
    private static final long serialVersionUID = 7311211439427836072L;

    private ProductDto product;
    private Integer level;
}
