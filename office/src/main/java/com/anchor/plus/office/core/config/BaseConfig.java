package com.anchor.plus.office.core.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = "com.anchor.plus.office")
@EnableJpaRepositories(basePackages = "com.anchor.plus.office")
@RequiredArgsConstructor
public class BaseConfig {
}
