import React from 'react';
import './css/App.css';
import axios from "axios";
import VideoPage from "./video/VideoPage";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import SchedulerPage from "./scheduler/SchedulerPage";
import "../src/css/header.css";


export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clientName: ''
        }
    }

    componentDidMount() {
        getName().then(resp => this.setState({clientName: resp.data}));
    }

    render() {
        return <Router>
            <div className="App" style={{paddingTop: '64px'}}>
                <Switch>
                    <Route path={'/operator-ui/calendar/:user'} component={SchedulerPage}/>
                    <Route path={'/operator-ui/meeting/:id'} component={VideoPage}/>
                </Switch>
            </div>
        </Router>
    }
}

export function getName() {
    return axios.get('/operator/name')
}

export default App;
