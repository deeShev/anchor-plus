import React, {useEffect, useRef, useState} from 'react';
import {Button} from "@material-ui/core";
import Video from "./Video";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import '../css/video.css'
import CardActions from "@material-ui/core/CardActions";
import {
    Fullscreen, FullscreenExit,
    Mic,
    MicOff,
    MicOutlined,
    PhotoCamera,
    ScreenShare,
    ScreenShareOutlined, StopScreenShare,
    StopScreenShareOutlined,
    Videocam,
    VideocamOff
} from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import {FullScreen, useFullScreenHandle} from "react-full-screen";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";

function VideoPlayer(props) {

    const dispatch = useDispatch();

    const [audioEnabled, setAudioEnabled] = useState(true);
    const [videoEnabled, setVideoEnabled] = useState(true);
    const [screenShared, setScreenShared] = useState(false);

    const fullScreenHandle = useFullScreenHandle();


    const audioOn = () => {
        props.localStream.getAudioTracks()[0].enabled = true;
        setAudioEnabled(true);
    }

    const audioOff = () => {
        props.localStream.getAudioTracks()[0].enabled = false;
        setAudioEnabled(false);
    }

    const videoOn = () => {
        props.localStream.getVideoTracks()[0].enabled = true;
        setVideoEnabled(true);
    }

    const videoOff = () => {
        props.localStream.getVideoTracks()[0].enabled = false;
        setVideoEnabled(false);
    }


    const shareScreen = () => {
        const {peerConnection} = props;
        const displayMediaOptions = {
            cursor: 'always'
        }
        navigator.mediaDevices.getDisplayMedia(displayMediaOptions).then(
            stream => {
                const sender = peerConnection.getSenders().find(sender => sender.track.kind === 'video');
                console.log('found sender:', peerConnection.getSenders());
                sender.replaceTrack(stream.getVideoTracks()[0]);
                setScreenShared(true);
            }).catch(
            error => {
                console.log(error)
            });
    }

    const stopScreenSharing = () => {
        const {peerConnection} = props;
        const sender = peerConnection.getSenders().find(sender => sender.track.kind === 'video');
        console.log('found sender:', peerConnection.getSenders());
        sender.replaceTrack(props.localStream.getVideoTracks()[0]);
        setScreenShared(false);
    }


    const finishMeeting = () => {

    }

    console.log(this.props)

    return (
        <FullScreen handle={fullScreenHandle}>
        <Card className={'player'}>
            <CardContent>
                    <div className={'video-container'}>
                        {!fullScreenHandle.active && <Video className={'localVideo'} srcobject={props.localStream}></Video>}
                        {/*{props.remoteStream ?*/}
                            <Video className={'remoteVideo'}
                                   style={fullScreenHandle.active ? {width: '75%'} : {}}
                                   srcobject={props.remoteStream}></Video>
                            {/*:*/}
                            {/*<div>*/}
                            {/*    <CircularProgress color='primary'/>*/}
                            {/*    <br />*/}
                            {/*    <Typography color='primary' variant="body1" noWrap style={{ minHeight: '50vh', padding: '0px 20px'}}>*/}
                            {/*Ожидайте подключения собеседника*/}
                            {/*    </Typography>*/}
                            {/*</div>*/}
                        }
                    </div>
            </CardContent>
            <CardActions>
                <Grid container>
                    <Grid item xs={4} />
                    <Grid item xs={4}>
                        <IconButton color="primary" component="span">
                            {audioEnabled ? <Mic onClick={audioOff}/> : <MicOff onClick={audioOn}/>}
                        </IconButton>
                        <IconButton color="primary" component="span">
                            {videoEnabled ? <Videocam onClick={videoOff}/> : <VideocamOff onClick={videoOn}/>}
                        </IconButton>
                        <IconButton color="primary" component="span">
                            {screenShared ? <StopScreenShare onClick={stopScreenSharing}/> : <ScreenShare onClick={shareScreen}/>}
                        </IconButton>
                        <IconButton color="primary" component="span">
                            {fullScreenHandle.active ? <FullscreenExit onClick={fullScreenHandle.exit}/> : <Fullscreen onClick={fullScreenHandle.enter}/>}
                        </IconButton>
                    </Grid>
                    <Grid style={{textAlign: 'end'}} item xs={4}>
                        <Link to={`'/operator-ui/calendar/${props.userId}`}>
                            Завершить
                        </Link>
                        {/*<Button color={'primary'} onClick={finishMeeting}>*/}
                        {/*    Завершить*/}
                        {/*</Button>*/}
                    </Grid>
                </Grid>
            </CardActions>
        </Card>
        </FullScreen>
    )
}

export default VideoPlayer;