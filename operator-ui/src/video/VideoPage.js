import React, {useEffect, useState} from 'react';
import {VideoRoom} from "./VideoRoom";
import {useParams} from "react-router";
import VideoPlaceholder from "./VideoPlaceholder";
import { useSelector, useDispatch } from 'react-redux';
import {
    getMeeting,
    selectAppointment,
} from './slice/videoPageSlice';
import {selectUser} from "../app/appSlice";
import Header from "../header/header";

function VideoPage(props) {
    const appointment = useSelector(selectAppointment);
    const user = useSelector(selectUser);
    const {id} = useParams();
    const dispatch = useDispatch();


    useEffect(() => {
        dispatch(getMeeting(id));
    }, [])

    const getPlaceholderText = () => {
        if (!appointment || !user) {
            return 'Загружаем встречу';
        }

        // if (!ready(appointment)) {
        //     return `Встреча начнется в ${appointment.dateTime}. Пожалуйста, подключитесь позже`
        // }
        //
        // function ready(appointment) {
        //     return new Date().getTime() - appointment.dateTime <= 600000;
        // }

    }

    const placeholderText = getPlaceholderText();

    return (
        <div >
            <Header/>
            {placeholderText ?
                <VideoPlaceholder loading text={placeholderText} />
                :
                <VideoRoom meeting={appointment} userId={user.employee.id}/>
            }
        </div>
    );
}

export default VideoPage;