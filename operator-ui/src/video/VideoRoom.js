import React, {useEffect, useState} from 'react';
import videoStream from "./VideoStream";
import ServerSocket from "../server/ServerSocket";
import {VideoChat} from "./VideoChat";
import Grid from "@material-ui/core/Grid";
import VideoPlayer from "./VideoPlayer";

export function VideoRoom(props) {

    const [localStream, setLocalStream] = useState();
    const [remoteStream, setRemoteStream] = useState();
    const [peerConnection, setPeerConnection] = useState();
    const [dataChannel, setDataChannel] = useState();

    const createPeerConnection = () =>{
        console.log(createPeerConnection)
        const configuration = null;

        const connection = new RTCPeerConnection(configuration, {
            optional : [ {
                RtpDataChannels : true
            } ]
        });

        // Setup ice handling
        connection.onicecandidate = function(event) {
            if (event.candidate) {
                ServerSocket.send({
                    event : "candidate",
                    data : event.candidate
                });
            }
        };

        connection.onaddstream = (remoteEvent) => setRemoteStream(remoteEvent.stream);

        // creating data channel
        const channel = connection.createDataChannel("dataChannel", {
            reliable : true
        });

        setPeerConnection(connection);
        setDataChannel(channel);
    }

    const connect = () => {
        if (!(localStream && peerConnection)) {
            return ;
        }
        ServerSocket.login(props.meeting.id, props.userId);
        ServerSocket.socket.onmessage = function (msg) {
            console.log("Got message", msg.data);
            var content = JSON.parse(msg.data);
            var data = content.data;
            switch (content.event) {
                // when somebody wants to call us
                case "offer":
                    handleOffer(data);
                    break;
                case "answer":
                    handleAnswer(data);
                    break;
                case "ready": {
                    handleCall()
                    break;
                }
                // when a remote peer sends an ice candidate to us
                case "candidate":
                    handleCandidate(data);
                    break;
                default:
                    break;
            }
        };
    }


    const handleCall = () => {
        const offerOptions = {
            offerToReceiveVideo: 1,
        };

        peerConnection.addStream(localStream);
        peerConnection.createOffer(offerOptions)
            .then(function (offer) {
                ServerSocket.send({
                    event: "offer",
                    data: offer
                });
                console.log("createdOffer", offer)
                peerConnection.setLocalDescription(offer)
            })
            .catch(e => alert("Error creating an offer"));
    }


    function handleOffer(offer) {
        console.log('handleOffer')
        peerConnection.setRemoteDescription(new RTCSessionDescription(offer));

        // create and send an answer to an offer
        peerConnection.addStream(localStream);
        peerConnection.createAnswer(function (answer) {
            peerConnection.setLocalDescription(answer);
            ServerSocket.send({
                event: "answer",
                data: answer
            });
        }, function (error) {
            alert("Error creating an answer");
        });

    };

    function handleCandidate(candidate) {
        console.log('handleCandidate')
        peerConnection.addIceCandidate(new RTCIceCandidate(candidate));
    };

    function handleAnswer(answer) {
        console.log('handleAnswer')
        peerConnection.setRemoteDescription(new RTCSessionDescription(answer));
        console.log("connection established successfully!!");
    };

    useEffect(() => {
        if (!localStream) {
            videoStream.createStream((stream) => {
                setLocalStream(stream)
            });
        } else {
            connect();
        }

    }, [localStream])

    useEffect(() => {
        if (!peerConnection) {
            createPeerConnection();
        } else {
            connect();
        }
    }, [peerConnection])

    useEffect(() => {
        return () => {
            ServerSocket.close();
        }
    }, [])
    return (
            <Grid container justify="center" spacing={2}>
                <Grid item xs={1} />
                <Grid item xs={7}>
                <VideoPlayer handleCall={handleCall} localStream={localStream}
                             remoteStream={remoteStream}
                             peerConnection={peerConnection}/>
                </Grid>
                <Grid className={'chat'} item xs={3}>
                    {dataChannel && <VideoChat datachannel={dataChannel}/>}
                </Grid>
                <Grid item xs={1} />
            </Grid>

    )
}