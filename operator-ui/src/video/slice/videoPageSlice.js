import { createSlice } from '@reduxjs/toolkit';
import * as Service from "../../service/Service";

export const videoPageSlice = createSlice({
    name: 'videoPage',
    initialState: {
        appointment: null,
    },
    reducers: {
        setAppointment: (state, action) => {
            state.appointment = action.payload;
        },
    },
});

export const { setAppointment } = videoPageSlice.actions;

export const getMeeting = id => dispatch => {
    Service.getMeeting(id).then(response => dispatch(setAppointment(response.data)));
};

export const selectAppointment = state => {
    return state.videoPage.appointment;
}

export default videoPageSlice.reducer;
