class VideoStream {

    // localStream;

    mediaStreamConstraints = {
        video: true,
        audio: true
    };

    createStream = (callback) => {
        console.log("create stream")
        let browserUserMedia = navigator.mediaDevices.getUserMedia(this.mediaStreamConstraints)
            .then(callback).catch(error => console.log(error))
    }

}

export default new VideoStream();