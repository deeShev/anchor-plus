import React, {useEffect} from "react"
import mainLogo from '../img/sber_logo_main.png';
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import {AiOutlineSearch, TiLocationArrow} from "react-icons/all";
import Grid from "@material-ui/core/Grid";
import {useDispatch, useSelector} from "react-redux";
import {loadUser, selectApp, updateOnline} from "../app/appSlice";
import {useParams} from "react-router";
import Switch from "@material-ui/core/Switch";




const Header = (props) => {

    const dispatch = useDispatch();
    const app = useSelector(selectApp)
    const {user} = useParams();

    const setOnline = () => {
        dispatch(updateOnline(app.user.employee.id, !app.online));
    }

    useEffect(() => {
        dispatch(loadUser(user))
    }, [])

    return <div>
        <AppBar position="fixed" className={'app-bar'}>
            <Grid container style={{justifyContent: 'center'}}>
                <Grid item xl={8} xs={10}>
                    <Toolbar>
                            <div
                                color="inherit"
                                aria-label="open drawer"
                                onClick={() => {
                                }}
                                className={'header-menu'}/>
                            <img src={mainLogo} className={'clickable'} style={{width: "105px", margin: "15px"}}
                                 alt="fireSpot"/>

                            <div style={{flex: '1 0', display: 'flex', justifyContent: 'flex-end', alignItems: 'center'}}>
                                <Typography component="div" style={{marginRight: "60px"}}>
                                    <Grid component="label" container alignItems="center" spacing={1}>
                                        <Grid item>Оффлайн</Grid>
                                        <Grid item>
                                            <Switch color={'primary'} checked={app.online} onChange={setOnline} name="checkedC" />
                                        </Grid>
                                        <Grid item>Онлайн</Grid>
                                    </Grid>
                                </Typography>
                                <Typography variant="body1">
                                    {app.user && `${app.user.employee.lastName} ${app.user.employee.firstName} ${app.user.employee.middleName}`}
                                </Typography>
                            </div>
                    </Toolbar>
                </Grid>
            </Grid>
        </AppBar>
    </div>
};

export default Header;