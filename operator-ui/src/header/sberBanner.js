import React from "react"
import Grid from "@material-ui/core/Grid";
import BannerImg from "../img/eco.c3d24d04.png";
import Typography from "@material-ui/core/Typography";

const Banner = (props) => {
    return <div>
        <div className={'banner'}>
            <Grid container alignItems={"center"}>
                <Grid item xs={12} sm={6}>
                    <Typography variant="h4" noWrap style={{padding: '0px 20px'}} className={'clickable'}>
                        Все возможности Сбера
                    </Typography>
                    <Typography variant="subtitle2" noWrap style={{padding: '0px 20px'}} className={'clickable'}>
                        Онлайн-сервисы для повседневной жизни и&nbsp;бизнеса
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <img src={BannerImg} className={'banner__image'} alt="fireSpot"/>
                </Grid>
            </Grid>

        </div>
    </div>
};

export default Banner;