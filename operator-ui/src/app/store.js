import { configureStore } from '@reduxjs/toolkit';
import videoPageReducer from '../video/slice/videoPageSlice';
import appReducer from './appSlice';


export default configureStore({
  reducer: {
    videoPage: videoPageReducer,
    app: appReducer
  },
});
