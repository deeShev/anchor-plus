import { createSlice } from '@reduxjs/toolkit';
import * as Service from "../service/Service";

function findCurrentMeeting(meetings) {
    return meetings.find(element => {
        const difference = Math.abs(new Date().getTime() - element.dateTime);
        console.log('time difference', difference)
        return difference <= 60000;
    });
}

export const appSlice = createSlice({
    name: 'app',
    initialState: {
        dialogOpened: false,
        meetings: [],
        currentMeeting: null,
        user: null,
        online: false
    },
    reducers: {
        setDialogOpened: (state, action) => {
            state.dialogOpened = action.payload;
        },
        setMeetings: (state, action) => {
            state.meetings = action.payload;
            const currentMeeting = findCurrentMeeting(state.meetings);
            if (currentMeeting) {
                state.currentMeeting = currentMeeting;
                state.dialogOpened = true;
            }
        },
        setUser: (state, action) => {
            state.user = action.payload;
        },

        setOnline: (state, action) => {
            state.online = action.payload;
        },
    },
});

export const { setMeetings, setDialogOpened, setUser, setOnline } = appSlice.actions;

export const loadEmployeeMeetings = id => dispatch => {
    Service.getEmployeeMeetings(id).then(response => dispatch(setMeetings(response.data)));
};

export const loadUser = id => dispatch => {
    Service.getEmployee(id).then(response => dispatch(setUser(response.data)));
};

export const updateOnline = (id, status) => dispatch => {
    Service.saveStatus(id, status).then(response => dispatch(setOnline(status)));
};

export const selectApp = state => {
    return state.app;
}

export const selectUser = state => {
    return state.app.user;
}

export default appSlice.reducer;
