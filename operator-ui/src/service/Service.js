import axios from "axios"

export function getMeeting(id) {
    return axios.get('/core/meeting-by-id', {params: {id}})
}

export function getEmployeeMeetings(id) {
    return axios.get('/core/meeting-by-employee-id', {params: {id}})
}

export function getEmployee(id) {
    return axios.get('/core/skills-by-employee-id', {params: {id}})
}

export function saveStatus(id, status) {
    return axios.get('/core/employee-update-new-status', {params: {id, status: status ? 1 : 0}})
}