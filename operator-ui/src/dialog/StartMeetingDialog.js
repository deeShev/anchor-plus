import React, {useEffect, useState} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {loadEmployeeMeetings, selectApp, setDialogOpened} from "../app/appSlice";

const StartMeetingDialog = (props) => {

    const app = useSelector(selectApp);
    const dispatch = useDispatch();

    const {currentMeeting} = app;


    const handleClose = () => {
        dispatch(setDialogOpened(false));
        props.history.push(`/operator-ui/meeting/${currentMeeting.id}`);
    }

    useEffect(() => {
        if (!app.user) {
            return;
        }
        const userId = app.user.employee.id;
        dispatch(loadEmployeeMeetings(userId))
        setInterval(() => dispatch(loadEmployeeMeetings(userId)), 3000);
    }, [app.user])

    return (
        currentMeeting && <Dialog
            open={app.dialogOpened}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Встреча с клиентом"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {`Клиент ${currentMeeting.client.lastName} ${currentMeeting.client.firstName} ${currentMeeting.client.middleName}
                ожидает на линии. \n Консультация по продукту: ${currentMeeting.product.name}`}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Принять
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default StartMeetingDialog;