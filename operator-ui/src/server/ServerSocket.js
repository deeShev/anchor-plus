class ServerSocket {
    socket;

    login = (meeting, userId) => {
        this.socket = new WebSocket('ws://localhost:8080/socket')
        this.socket.onopen = () => this.send({
            event: 'login',
            data: userId,
            meeting: meeting
        })
    }

    call = (destUser) => {
        this.send({
            event: 'call',
            data: destUser
        })
    }

    send = message => {
        this.socket.send(JSON.stringify(message))
        console.log('sended message ', JSON.stringify(message))
    }

    close = () => {
        this.socket.close();
    }
}

const instance = new ServerSocket();

export default instance;