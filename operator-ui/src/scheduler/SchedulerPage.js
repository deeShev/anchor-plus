import React, {useEffect, useState} from 'react';
import Header from "../header/header";
import {Grid} from "@material-ui/core";
// import Scheduler from "./Scheduler";
import {useDispatch, useSelector} from "react-redux";
import {selectApp} from "../app/appSlice";
import Typography from "@material-ui/core/Typography";
import Switch from "@material-ui/core/Switch";
import Toolbar from "@material-ui/core/Toolbar";
import StartMeetingDialog from "../dialog/StartMeetingDialog";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListSubheader from "@material-ui/core/ListSubheader";
import Paper from "@material-ui/core/Paper";
import MeetingCard from "./MeetingCard";

const SchedulerPage = (props) => {

    const dispatch = useDispatch();
    const app = useSelector(selectApp);

    const getSkills = () => {
        return app.user.skillDtos.map((item, i) => {
            return <ListItem key={i}>
                <ListItemText primary={item.product.name} />
            </ListItem>
        })
    }

    const goToVideo = (id) => {
        props.history.push(`/operator-ui/meeting/${id}`)
    }

    const getMeetings = () => {
        console.log(app.meetings)
        return app.meetings.map((item, i) => {
            return <MeetingCard key={i} meeting={item} onMeetingClick={() => goToVideo(item.id)} />
        })
    }

    return (
        <div style={{marginTop: '10px'}}>
            <Header load/>
            <StartMeetingDialog history={props.history}/>
            <Grid container spacing={2}>
                <Grid item xs={2}>

                </Grid>
                {/* <Grid item xs={9}>*/}
                <Grid item xs={8}>
                    {app.user &&
                    <Typography variant="h5" gutterBottom>
                        {app.meetings.length ? 'Встречи на сегодня' : 'Встреч на сегодня еще нет'}
                    </Typography>
                    }
                    {app.meetings && getMeetings()}
                </Grid>
                <Grid item xs={2}>
                    <Typography variant="h5" gutterBottom>
                        Мои навыки
                    </Typography>
                    <Paper>
                        {app.user && getSkills()}
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )

}

export default SchedulerPage;