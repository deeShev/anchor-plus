import React, {useEffect, useState} from 'react';
import Header from "../header/header";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";


const MeetingCard = (props) => {
    return <Card variant="outlined">
        <CardContent>
            <Typography style={{textAlign: "left"}} gutterBottom>
                <p>{new Date(props.meeting.dateTime).toLocaleDateString()} {new Date(props.meeting.dateTime).toLocaleTimeString()}</p>
            </Typography>
            <Typography variant="h5" component="h5" style={{textAlign: "left"}}>
                <p>Продукт: {props.meeting.product.name}</p>
            </Typography>
            <Typography variant="h5" component="h3" style={{textAlign: "left"}}>
                <p>Клиент: {props.meeting.client.lastName} {props.meeting.client.firstName}</p>
            </Typography>
        </CardContent>
        <CardActions>
            <Button size="small" onClick={props.onMeetingClick}
                    style={{backgroundColor: "#19bb4f", color: '#fff', fontFamily: "sans-serif"}}>Подключиться</Button>
        </CardActions>
    </Card>
}

export default MeetingCard;