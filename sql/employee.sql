CREATE TABLE "employee" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    first_name  TEXT,
    middle_name TEXT,
    last_name   TEXT,
    status      INTEGER
);

INSERT INTO employee (first_name, middle_name, last_name, status) VALUES
(
 'Виктория',
 'Альбертовна',
 'Семенова',
 0
);

INSERT INTO employee (first_name, middle_name, last_name, status) VALUES
(
    'Анастасия',
    'Андреевна',
    'Панаева',
    0
);

INSERT INTO employee (first_name, middle_name, last_name, status) VALUES
(
    'Светлана',
    'Петровна',
    'Кочнева',
    1
);

INSERT INTO employee (first_name, middle_name, last_name, status) VALUES
(
    'Мария',
    'Ивановна',
    'Григорьева',
    1
);

INSERT INTO employee (first_name, middle_name, last_name, status) VALUES
(
    'Вера',
    'Дмитриевна',
    'Гладкова',
    1
);

INSERT INTO employee (first_name, middle_name, last_name, status) VALUES
(
    'Надежда',
    'Владимировна',
    'Авербух',
    1
);