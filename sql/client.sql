CREATE TABLE "client" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    first_name  TEXT,
    middle_name TEXT,
    last_name   TEXT,
    login       TEXT
);

INSERT INTO client (FIRST_NAME, MIDDLE_NAME, LAST_NAME, login) VALUES (
'Николай', 'Константинович', 'Писнячевский','nikolay');

INSERT INTO client (FIRST_NAME, MIDDLE_NAME, LAST_NAME, login) VALUES (
'Денис', '', 'Шевелев','denis');

INSERT INTO client (FIRST_NAME, MIDDLE_NAME, LAST_NAME, login) VALUES (
'Андрей', 'Валерьевич', 'Драный','andrey');

INSERT INTO client (FIRST_NAME, MIDDLE_NAME, LAST_NAME, login) VALUES (
'Александра', 'Ивановна', 'Кононова','alexandra');