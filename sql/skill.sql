CREATE TABLE "skill" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   product_id INTEGER NOT NULL,
   employee_id INTEGER NOT NULL,
   level INTEGER,
   FOREIGN KEY (product_id) REFERENCES product("id"),
   FOREIGN KEY (employee_id) REFERENCES employee("id")
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
7, 1, 10--инвестиции
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
8, 1, 10--вклады
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
9, 1, 10--кредиты
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
12, 2, 10--здоровье
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
15, 3, 10--okko
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
20, 3, 9--delivery club
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
10, 4, 9--выд кредиты
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
11, 4, 8--сберкредо
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
16, 4, 7--РКО
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
17, 4, 8--бухгалтерия
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
22, 5, 9--ку
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
23, 5, 8--сберкласс
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
18, 6, 8--юрист
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
19, 6, 8--проверка
);

INSERT INTO skill (product_id, employee_id, level) VALUES (
21, 6, 8--сервис
);
