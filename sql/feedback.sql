CREATE TABLE "feedback" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   meeting_id INTEGER NOT NULL,
   csi INTEGER,
   comment TEXT,
   FOREIGN KEY (meeting_id) REFERENCES meeting("id")
);
